<?php


include('db.php');


if(isset($_POST['valider'])) {

    if(!empty($_POST['email']) AND !empty($_POST['password']) AND !empty($_POST['password_conf']) AND !empty($_POST['age'])) {

        $verif_utilisateurs = $bdd->prepare('SELECT email FROM utilisateurs WHERE email = ?');
        $verif_utilisateurs->execute(array($_POST['email']));
        $utilisateur_exist = $verif_utilisateurs->rowCount();

        if($utilisateur_exist === 0) {

            if($_POST['password'] == $_POST['password_conf']) {

                if($_POST['age'] >= 18) {

                    $password_crypted = password_hash($_POST['password'], PASSWORD_BCRYPT);

                    $insertion_utilisateur = $bdd->prepare("INSERT INTO utilisateurs (email, password, age, administrateur) VALUES (?,?,?,?)");
                    $insertion_utilisateur->execute(array($_POST['email'], $password_crypted, $_POST['age'], '0'));

                    header('Location: connexion.php');

                } else { ?>
                    <p class="message">Vous devez avoir plus de 18 ans</p>
                <?php
                }
            } else {?>
                <p class="message">Les mots de passe ne correspondent pas</p>
            <?php
            }
        } else {?>
            <p class="message">Email déjà utilisé</p>
        <?php
        }
    } else {?>
        <p class="message">Tous les champs doivent être rempli</p>
    <?php
    }

}

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/inscription.css">
    <title>Inscription</title>
</head>
<body>
    <div>
        <ul class="topnav">
            <li class="inscription">
                <div class="select"><a class="button" href="inscription.php">Inscription</a></div>
            </li>
            <li>
                <div><a class="button" href="connexion.php">Connexion</a></div>
            </li>
            <li>
                <div><a class="button" href="panier.php">Panier</a></div>
            </li>
            <li>
                <div><a class="carte" href="carte.php">Carte</a></div>
            </li>
            <a href="index.php">
                <img class="logo" src="Capture_2.png">
            </a>
            <li>
                <div><a class="histoire" href="https://fr.wikipedia.org/wiki/Cit%C3%A9_du_Vin">Histoire</a>
                </div>
            </li>
            <li>
                <div><a class="accueil" href="index.php">Accueil</a></div>
            </li>
        </ul>
    </div>
    <div class="general">
        <form method="POST">
            <input class="email" type="text" placeholder="Email" name="email">
            <input class="password" type="password" placeholder="Password" name="password">
            <input class="conf-password" type="password" placeholder="Confirmation password" name="password_conf">
            <input class="age" type="text" placeholder="Entrez votre age" name="age">
            <input class="valider" type="submit" value="Valider" name="valider">
        </form>
    </div>
</body>
</html>