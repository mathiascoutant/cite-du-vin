<?php

include('db.php');


if(isset($_POST['valider'])) {

    if(!empty($_POST['email']) AND !empty($_POST['password'])) {

        $verif_email = $bdd->prepare('SELECT * FROM utilisateurs WHERE email = ?');
        $verif_email->execute(array($_POST['email']));
        $user = $verif_email->fetch();
        $email_exist = $verif_email->rowCount();

        if($email_exist === 1) {

            if(password_verify($_POST['password'], $user['password'])) {

                $_SESSION['id'] = $user['id'];

                header('Location: index.php');

            } else { ?>
                <p class="message">Mauvais mot de passe</p>
            <?php
            }
        } else { ?>
            <p class="message">Email incorrect</p>
        <?php
        }
    } else { ?>
        <p class="message">Tous les champs doivent être rempli</p>
    <?php
    }
}

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/connexion.css">
    <title>Connexion</title>
</head>
<body>
    <div>
        <ul class="topnav">
            <li class="inscription">
                <div><a class="button" href="inscription.php">Inscription</a></div>
            </li>
            <li>
                <div class="select"><a class="button" href="connexion.php">Connexion</a></div>
            </li>
            <li>
                <div><a class="button" href="panier.php">Panier</a></div>
            </li>
            <li>
                <div><a class="carte" href="carte.php">Carte</a></div>
            </li>
            <a href="index.php">
                <img class="logo" src="Capture_2.png">
            </a>
            <li>
                <div><a class="histoire" href="https://fr.wikipedia.org/wiki/Cit%C3%A9_du_Vin">Histoire</a>
                </div>
            </li>
            <li>
                <div><a class="accueil" href="index.php">Accueil</a></div>
            </li>
        </ul>
    </div>
    <div class="general">
        <form method="POST">
            <input class="email" type="text" placeholder="Email" name="email">
            <input class="password" type="password" placeholder="Mot de passe" name="password">
            <br>
            <input class="valider" type="submit" value="Valider" name="valider">
        </form>
    </div>
</body>
</html>