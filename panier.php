<?php

include('db.php');


if(isset($_SESSION['id'])) { ?>
            <!DOCTYPE html>
            <html lang="fr">
            <head>
                <meta charset="UTF-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <link rel="stylesheet" href="/css/panier.css">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>Document</title>
            </head>
            <body>
                <div>
                    <ul class="topnav">
                        <li class="inscription">
                            <div><a class="button" href="inscription.php">Inscription</a></div>
                        </li>
                        <li>
                            <div><a class="button" href="connexion.php">Connexion</a></div>
                        </li>
                        <li>
                            <div class="select"><a class="button" href="panier.php">Panier</a></div>
                        </li>
                        <li>
                            <div><a class="carte" href="carte.php">Carte</a></div>
                        </li><?php
                        if($_SESSION['id']){
                            $verif_admin= $bdd->prepare('SELECT * FROM utilisateurs WHERE id = ?');
                            $verif_admin->execute(array($_SESSION['id']));
                            $user = $verif_admin->fetch();

                            if($user['administrateur'] == 1) { ?>
                                <li>
                                    <div><a class="ajout-vin" href="ajout_vin.php">Ajouter vin</a></div>
                                </li> <?php
                            }
                        } ?>
                        <a href="index.php">
                            <img class="logo" src="Capture_2.png">
                        </a>
                        <li>
                            <div><a class="histoire" href="https://fr.wikipedia.org/wiki/Cit%C3%A9_du_Vin">Histoire</a>
                            </div>
                        </li>
                        <li>
                            <div><a class="accueil" href="index.php">Accueil</a></div>
                        </li>
                    </ul>
                </div> 
            </body>
            </html>
                <?php

    $select_vins = $bdd->query('SELECT * FROM panier WHERE id > 0 AND user_panier = "'.$_SESSION['id'].'"');
    $panier_vide = $select_vins->rowCount();

    if($panier_vide >= 1) {

        while($vins_panier = $select_vins->fetch()) { 


            $verif_vin = $bdd->prepare('SELECT * FROM vins WHERE id = ?');
            $verif_vin->execute(array($vins_panier['id_vin']));
            $vin = $verif_vin->fetch(); 
            
            if(isset($_GET['suppr'])) {

                $delete_vin_panier = $bdd->prepare('DELETE FROM panier WHERE id_vin = ?');
                $delete_vin_panier->execute(array($_GET['suppr']));

                header('Location: panier.php');

            }
            
            ?>
            <div class="espace1"></div>
            <div class="general">
                <img class="bouteilleVin" src="images/<?php echo $vin['nom_image']; ?><?php echo $vin['extension_image']; ?>" alt="">
                <p class="nomVin"><?php echo $vin['nom_vin']; ?></p>
                <p class="prixVin"><?php echo $vin['prix_vin']; ?> €</p>
                <form method="GET">
                    <a class="suppr" href="panier.php?suppr=<?php echo $vin['id'];?>" >Supprimer</a>
                </form>
                <br>
            </div> <?php
        }
    } else { ?>
        <p class="panier-vide">Vous n'avez pas de vin dans votre panier</p><?php
    }
}



?>