<?php

include('db.php');


if(isset($_SESSION['id'])) { 

    if(!empty($_GET['id'])){

        $verif_vin = $bdd->prepare('SELECT * FROM vins WHERE id = ?');
        $verif_vin->execute(array($_GET['id']));
        $vin = $verif_vin->fetch();
        $vin_exist = $verif_vin->rowCount();
        
        if($vin_exist == 1) {
            
            if(isset($_POST['ajout_panier'])){

                $insertion_vin_panier = $bdd->prepare("INSERT INTO panier (id_vin, user_panier) VALUES (?,?)");
                $insertion_vin_panier->execute(array($vin['id'], $_SESSION['id']));

                header('Location: vin.php?id='.$vin['id']);

            }
            
            
            ?>
    
            <!DOCTYPE html>
            <html lang="fr">
            <head>
                <meta charset="UTF-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <link rel="stylesheet" href="/css/vin.css">
                <link href="http://fonts.cdnfonts.com/css/stardos-stencil" rel="stylesheet">
                <link href="http://fonts.cdnfonts.com/css/allejo" rel="stylesheet">
                <title>Vin <?php echo $vin['nom_vin'] ?></title>
            </head>
            <body>
                <div>
                    <ul class="topnav">
                        <li class="inscription">
                            <div><a class="button" href="inscription.php">Inscription</a></div>
                        </li>
                        <li>
                            <div><a class="button" href="connexion.php">Connexion</a></div>
                        </li>
                        <li>
                            <div><a class="button" href="panier.php">Panier</a></div>
                        </li>
                        <li>
                            <div><a class="carte" href="carte.php">Carte</a></div>
                        </li><?php
                        if($_SESSION['id']){
                            $verif_admin= $bdd->prepare('SELECT * FROM utilisateurs WHERE id = ?');
                            $verif_admin->execute(array($_SESSION['id']));
                            $user = $verif_admin->fetch();

                            if($user['administrateur'] == 1) { ?>
                                <li>
                                    <div><a class="ajout-vin" href="ajout_vin.php">Ajouter vin</a></div>
                                </li> <?php
                            }
                        } ?>
                        <a href="index.php">
                            <img class="logo" src="Capture_2.png">
                        </a>
                        <li>
                            <div><a class="histoire" href="https://fr.wikipedia.org/wiki/Cit%C3%A9_du_Vin">Histoire</a>
                            </div>
                        </li>
                        <li>
                            <div class="select"><a class="accueil" href="index.php">Accueil</a></div>
                        </li>
                    </ul>
                </div>
                <div class="block-vin">
                    <div class="block-img">
                        <img class="img-vin" src="images/<?php echo $vin['nom_image']; ?><?php echo $vin['extension_image']; ?>" alt="">
                    </div>
                    <div class="block-info">
                        <p class="nom_vin"><?php echo $vin['nom_vin']; ?></p>
                        <p class="prix_vin"><?php echo $vin['prix_vin']; ?> €</p>
                        <form method="post">
                            <input class="ajout-panier" type="submit" name="ajout_panier" value="Ajouter au panier">
                        </form>
                    </div>
                </div>
                <div class="block-desc">
                    <p class="desc">Description du vin :</p>
                    <p class="desc_vin"><?php echo $vin['description']; ?></p>
                </div>
                <div class="fin"></div>
            </body>
            </html> <?php
        
        }
    }
}
?>