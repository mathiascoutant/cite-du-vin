<?php

include('db.php');

if(isset($_POST['med'])) { ?>

    <h3>Médoc:</h3>
    <h5>Climat:</h5>
    <p>Entre Océan Atlantique et Gironde, le vignoble du Médoc est soumis à un climat tempéré océanique <br>doux et humide.La forêt des Landes protège le vignoble du Médoc des vents venant de la côte<br> aquitaine. L'océan Atlantique, l'estuaire de la Gironde et la vaste forêt landaise dégagent une grande <br>quantité de vapeur d'eau qui régule et limite l'amplitude annuelle des températures.<br></p>
    
    <h5>Situation des vignoble du Médoc:</h5>
    <p>La région viticole du Médoc est située au nord-ouest du département de la Gironde. Elle s'étend du<br> nord au sud sur une longueur de 80 kilomètres allant de la commune  de Saint-Vivien-de-Médoc <br>établie à quelques kilomètres de la Pointe de Grave à  la commune de Blanquefort en limite de <br>l'agglomération bordelaise et sur largeur d'une dizaine de kilomètres limitée à l'est par l'estuaire de la<br> Gironde au cours orienté sud-est, nord-ouest et par  l'immense forêt landaise de pins sylvestres<br> à l'ouest qui la sépare de l'océan Atlantique.<br>
       La zone viticole occupe les terrasses alluviales modelées en croupes par l'érosion de la rive gauche<br>de la Garonne de Blanquefort au Bec d'Ambès, point de sa confluence avec la Dordogne, et celles de <br> la rive gauche de la Gironde du Bec d'Ambès à Saint-Vivien-de-Médoc.<br></p>
    
    <h5>Production et superficie</h5>
    <table>
        <tr>- Médoc : 290 000 hl/an</tr>
        <br><tr>- Haut-Médoc : 250 000 hl/an</tr></br>
        <tr>- 16 000 ha</tr>
        <br><tr>- Médoc : 4900 ha</tr></br>
        <tr>- Haut-Médoc : 4400 ha</tr>
        <h5>Cépages :</h5>
        <tr>- Cabernet-sauvignon 54%,</tr>
        <br><tr>- Merlot 40%,</tr></br>
        <tr>- Cabernet franc 5%,</tr>
        <br><tr>- Petit Verdot 1%.</tr></br>
        
    </table>   

    <h5>Les sols du Médoc</h5>
    <p>Deux types de sols sont présents sur ces formations géologiques:<br>
       <br><strong>Sols graveleux dans le Haut-Médoc</strong> , sur les croupes et les plateaux couverts de graves alluviales.<br> L'épaisseur des graves avec une texture grossière favorise le drainage des eaux de pluie. La vigne<br> enfonce ses racines jusqu'à 5 m. On y trouve tous les Grands Crus classés.<br>
       <br><strong>Sols bruns argilo-calcaires ou  argilo-sableux</strong> sur les affleurements calcaires cénozoiques à <br>Moulis, Listrac, Saint-Estèphe, Saint Sauveur, Vertheuil, Cissac, Couquèques Valeyrac , Bégadan... <br>Les sols sont constitués d'argiles, de limons et de sables en proportion variable selon l'endroit.<br></p>

<?php
}

if(isset($_POST['gra'])) { ?>

    <h3>Graves :</h3>
    <h5>Climat:</h5>
    <p>Climat océanique tempéré.  La forêt des Landes protége le vignoble des pluies et des vents venant de <br>l'océan.<br>
       La Garonne apporte sa douceur.<br></p>
    <h5>Situation des vignoble de Graves:</h5>
    <p>La région des Graves longe à l'est la rive gauche de la Garonne de Eysines au nord de la ville de Bordeaux <br> à Saint-Pardon-de-Conques au sud-est de Langon. 
    Le vignoble s'étend sur environ 50 <br>kilomètres de long et 15 kilomètres de large, il enclave le vignoble de Cérons, Barsac et Sauternes.<br> A l'ouest, la forêt des Landes le sépare de l'Océan Atlantique.<br>
    <br>La région doit son nom à la composition de son sous-sol fait de graves.</p>
    <h5>Production et superficie:</h5>
    <table>
    <tr>Rouge (75 %),</tr>
    <br><tr>Blancs secs (20 %),</tr></br>
    <tr>Liquoreux (10%)</tr>
    </table> 
    <h5>Cépages :</h5>
    <p>Cabernet sauvignon et merlot sont la base des vins rouges, sauvignon et sémillon sont la base des <br>vins blancs</p>
    <h5>Les sols de Graves:</h5>
    <p>La constitution du sol est variable du nord au sud de l’appellation : graves et sables ou sables, argiles<br> et limons ou simplement sables argileux.<br>
       Les sols sont tantôt :<br>
       argilo-graveleux , siliceux, formé de sables, de silex , de graves de différentes de taille ( graviers,<br> cailloux, galets, sables grossiers).<br>
       argilo-calcaires<br>
       et parfois constitué d’alios,cimentation des grains de sable et graviers par des hydroxydes de fer,<br> d’aluminium et de manganèse.</p>
<?php
}

if(isset($_POST['entre'])) { ?>

    <h3>Entre deux Mers :</h3>
    <h5>Climat:</h5>
    <p>Climat océanique tempéré, doux, brouillards matinaux favorables aux vins blancs moelleux et<br> liquoreux. Les vignobles des hauts plateaux ( altitude pouvant aller jusqu'à 140 mètres) connaissent<br> des températures plus froides.<br></p>
    <h5>Situation des vignoble de l'Entre-De-Mers:</h5>
    <p>La région géographique de l'Entre-Deux-Mers couvre un triangle dont le sommet est le Bec d'Ambés,<br> les côtés sont la Dordogne au nord, la Garonne au sud-ouest, et la base la limite du département à<br> l'est.
       L'Entre-Deux-Mers est un vaste plateau , en légère pente vers le sud-ouest, découpé par les<br> rivières et l'érosion en  un  paysage de collines, de coteaux, de plateaux et de vallées qui s'étendent <br>sur 60 km de long et 30 km de large et dont l'altitude varie de 5 à 140m<br></p>
        <h4>Huit vignobles et dix appellations composent la région de l'Entre-Deux-Mers appelée aussi entre Dordogne et Garonne : </h4>
        <table>
        <ol>
            <li> Le vignoble de la région de l'Entre-Deux-Mers du même nom que la région géographique.</li>
            <li> Le vignoble de la région des Graves de Vayres</li>
            <li> Le vignoble de la région des Premières Côtes de Bordeaux</li>
            <li> Le vignoble de la région des Premières Côtes de Bordeaux</li>
            <li> Le vignoble de la région de Loupiac</li>
            <li> Le vignoble de la région de Sainte-Croix-du-Mont</li>
            <li> Le vignoble de la région des Côtes de Bordeaux Saint-Macaire</li>
            <li> Le vignoble de la région de Sainte-Foy Bordeaux</li>
        </ol>
        </table>
       <h5>Production et superficie:</h5>
    <table>
    <tr><strong>Superficie : 7 500 hectares.</strong></tr>
    <br><tr>Entre-deux-Mers : 1 500 ha</tr>
    <br><tr>Entre-deux-mers Haut-Benauge : 200 ha</tr>
    <br><tr>Graves de Vayres : 500 ha</tr>
    <br><tr>Cadillac Côtes de Bordeaux : 2 653 ha</tr>
    <br><tr>Premières Côtes de Bordeaux : 190 ha</tr>
    <br><tr>Cadillac : 200 ha</tr>
    <br><tr>Loupiac : 300 ha</tr>
    <br><tr>Sainte-Croix-du-Mont : 370 ha</tr>
    <br><tr>Côtes de Bordeaux-Saint-Macaire : 50 ha</tr>
    <br><tr>Sainte-Foy-Bordeaux : 750 ha</tr></br>

    <br><tr><strong>Production :  400 000 hl/an</strong></tr>
    <br><tr>Entre-deux-Mers : 90 000 hl/an</tr>
    <br><tr>Entre-deux-mers Haut-Benauge : 11 000 hl/an</tr>
    <br><tr>Graves de Vayres : 33 000 hl/an</tr>
    <br><tr>Cadillac Côtes de Bordeaux :100 298 hl/an</tr>
    <br><tr>Premières Côtes de Bordeaux : 7 000 hl/an</tr>
    <br><tr>Cadillac : 5 000 hl/an</tr>
    <br><tr>Loupiac : 10 000 hl/an</tr>
    <br><tr>Sainte-Croix-du-Mont : 12 000 hl/an</tr>
    <br><tr>Côtes de Bordeaux-Saint-Macaire : 2000 hl/an</tr>
    <br><tr>Sainte-Foy-Bordeaux : 13 700 hl/an</tr>

    </table> 
    <h5>Les sols de l'Entre-De-Mers:</h5>
    <p>Graveleux et argileux sur les graves garonnaises  épaisses de 2 à 10 m , dans les communes <br>de Premières-Côtes-de-Bordeaux, de Créon, de la Sauve et dans le vignoble des Graves-de-Vayres.<br>
       argileux sur la molasse  de l'Agenais. La molasse affleure  au sommet des coteaux des Premières<br>-Côtes-de-Bordeaux, des collines de Rauzan, Castelviel,   Soussac. et de Sauveterre.<br>
       Bruns calcaires  sur le calcaire à astéries.<br>
       Le calcaire à astéries est la principale formation du sous-bassement de la région. En légère pente vers <br>le sud-ouest, il forme le sommet et les pentes des plateaux, on le trouve  proche de l'affleurement sur<br> les plateaux de Rauzan, de Grézillac,  en sommet de côte sur la rive gauche de la Dordogne et à la<br> base de la falaise qui longe la Garonne à Sainte-Croix-du-Mont. L'épaisseur des sols calcaires sur ce <br>substrat est variable (50 cm à 1.50 mètres).<br>
       De boulbènes. Ce sont des sols   limono-argileux, limono-sableux, profonds, très fertile qui se sont<br> développés sur des alluvions fines de la Garonne et des molasses de l'Agenais<br></p>

<?php
}

if(isset($_POST['lib'])) { ?>

    <h3>Libournais :</h3>
    <h5>Climat:</h5>
    <p>Proche de l’Océan Atlantique, le vignoble du Libournais est soumis à un climat tempéré océanique <br>doux et humide, mais l'influence continentale est présente.<br>
       La proximité de la Dordogne, de son affluent l’Isle, de la rivière la Barbanne et de nombreux<br> ruisseaux renforce la protection du vignoble contre les gelées.<br></p>

    <h5>Situation des vignoble de la Haute-Gironde:</h5>
    <p>La région du Libournais regroupe autour de la ville de Libourne et le long de la rive droite de la <br>Dordogne et de son affluent l'Isle les vignobles de :<br><br>
    <br><strong>Saint-émilion, et ses satellites :</strong> (Montagne-Saint-Emilion, Saint-georges-Saint-Emilion, Lussac<br> Saint-Emilion, Puisseguin Saint-Emilion)<br>
       Le vignoble de la région de Saint-émilion commence aux portes de Libourne. Il s'étend le long de la<br> rive droite de la Dordogne sur une longueur de 8 kilomètres et une largeur de 5 kilomètres.
       <br><strong>Pomerol, Lalande de Pomerol.</strong><br>
       Le vignoble commence aux portes de Libourne sur la rive droite de la Dordogne et côtoie au nord-est <br>les vignobles de Saint-émilion .
       <br> <strong>Fronsac, Canon-Fronsac.</strong><br>
       Le vignoble de la région de Fronsac s'étend au nord-ouest de Libourne entre les rives de la Dordogne<br> et de l'Isle.
       <br> <strong>Castillon Côtes de Bordeaux.</strong><br>
       Le vignoble de la région de Castillon s'étend autour de la ville de Castillon -la-Bataille entre la rive <br>droite de la Dordogne,<br> les vignobles de Saint-émilion, et le département de la Dordogne.
       <br><strong>et Francs Côtes de Bordeaux.</strong><br>
       Le vignoble de la région des Côtes de Francs est une enclave située au nord des Côtes de Castillon<br> entre le département de la Dordogne et les vignobles de Lussac et Puisseguin Saint-émilion.</p>
    
       <h5>Encépagement :</h5>
    <p>Principalement des cépages à vins rouges.<br>
       Les cépages à vins blancs ne sont utilisés que dans l'appellation Côtes de BordeauxFrancs.<br>
       Le Merlot est le cépage dominant dans l'élaboration des vins du Libournais.<br></p> 
    <h5>Les sols du Libournais:</h5>
    <p>On distingue plusieurs types de sols :
       calcaires sur le calcaire à astéries du plateau et des versants.<br>
       argilo-calcaires sur la molasse du Fronsadais sur les coteaux.<br>
       sablo-graveleux, argilo-siliceux, argilo-graveleux sur les graves quaternaires.<br>
       sablo-argileux ou sablo-limoneux avec des graviers sur les sables et graviers du Périgord.</p>

<?php
}

if(isset($_POST['haute'])) { ?>

    <h3>Haute-Girondes (Le Blayais et le Bourgeais</h3>
    <h5>Climat:</h5>
    <p>Le climat du Blayais - Bourgeais est de type océanique marqué par des hivers doux et des<br> températures estivales plutôt chaudes. <br>
    <br><strong>Dans le Blayais</strong>, la proximité de l'estuaire de la Gironde, avec son  énorme masse d'eau, adoucit <br> la température et crée un microclimat plus doux encore dans les zones  viticoles située  le long de<br> l'estuaire.<br>
    <br><strong>Dans le Bourgeais</strong>, le vignoble est planté sur des coteaux aux expositions variées.<br> On est en présence de nombreux microclimats.</p>
    <h5>Situation des vignoble de la Haute-Gironde:</h5>
    <p><strong>Le vignoble du Blayais</strong> est situé dans la partie nord du département de la Gironde sur la rive droite <br>de l’estuaire de la Gironde, il fait face au vignoble du Médoc situé en rive gauche. L'aire géographique <br>concerne 41 communes réparties sur trois cantons:<br>  canton de Blaye, canton de Saint-Ciers-sur-Gironde et canton de Saint-Savin.<br>
       Dans sa partie sud-ouest, le vignoble côtoie le vignoble des Côtes de Bourg et gravite autour la <br>citadelle de Blaye dans un paysage de collines et de plateaux qui longent et surplombent l'estuaire.<br>
       Au nord et à l'est; il s'étend dans l"arrière pays jusqu'aux limites du département de la Gironde sur un<br> relief  ondulé, aux pentes douces et sablonneuses.<br>
       <br><strong>Le vignoble du Bourgeais</strong> s'étale sur environ 15 km le long de la rive droite de la Gironde et de <br>la rive droite de la Dordogne dans un paysage de collines sur des coteaux parallèles au cours du fleuve, <br>il fait face au vignoble du Médoc situé en rive gauche de l'estuaire. L'aire géographique concerne<br> quinze communes viticoles autour de la commune de Bourg sur Gironde (Bourg sur la carte). Au nord<br> et à l'est, le vignoble côtoie de vignoble de Blaye. La rivière: « Le Brouillon » le  sépare au nord-ouest<br>du vignoble du Blayais.</p>
    <h5>Production et superficie:</h5>
    <p><strong>Blayais : 340 000 hl/an</strong>
    Vins rouges sur les sols argilo-calcaires. (97%) de la<br> production 290 000 hl<br>
    Vins blancs secs sur les sols siliceux.10 000 hl<br>
    </p> 
    <p><strong>Bourgeais : 230 000 hl/an (190 000 hl en 2010)</strong>
    Vins rouges:  (99%) de la production.<br> 228 000 hl<br>
    Un peu de vins blancs. Environ 1400 hl<br>

    </p> 
    <h5>Les sols de la Haute-Gironde:</h5>
    <p><strong>Les sols du Blayais</strong>
       Selon la zone géographique de l'appellation,  les sols peuvent être: argilo-calcaires, argilo-graveleux,<br> silico-argileux.<br>
       Dans le sud-ouest sous une ligne qui va d’Anglade à Saint-Vivien-de-Blayeur, dans le pays des <br>collines sur le socle calcaire, les sols sont bruns, peu argileux et peu épais, souvent de type rendzine.<br>
       Dans l’intérieur du plateau, les sols bruns deviennent plus argileux et à tendance sableuse dans les<br> pentes.<br>
       Au nord et  à l'est de l’aire géographique, pays des sables et des graviers, les sols localement <br>sablo-graveleux sont majoritairement sableux gris à noirs, acides.<br> </p>

<?php
}


?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="/css/carte.css" rel="stylesheet">
    <title>Carte</title>
</head>
<body>
    <img src="carte.jpeg" alt="">
    <form method="post">
        <input class="med" type="submit" value="" name="med">
        <input class="gra" type="submit" value="" name="gra">
        <input class="lib" type="submit" value="" name="lib">
        <input class="haute" type="submit" value="" name="haute">
        <input class="entre" type="submit" value="" name="entre">
    </form>
</body>
</html>