<?php


include('db.php');

if(isset($_GET['user'])) {

    $verif_user= $bdd->prepare('SELECT id FROM utilisateurs WHERE id = ?');
    $verif_user->execute(array($_SESSION['id']));
    $user = $verif_user->fetch(); 
    $user_exist = $verif_user->rowCount(); 

    if($user_exist === 1) {

        session_destroy();

        header('Location: index.php');

    }

}

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/style.css">
    <title>Accueil</title>
</head>

<body>
    <div>
        <ul class="topnav"> <?php
            if(!empty($_SESSION['id'])){
                $verif_admin= $bdd->prepare('SELECT * FROM utilisateurs WHERE id = ?');
                $verif_admin->execute(array($_SESSION['id']));
                $user = $verif_admin->fetch(); ?>

                <li class="profil">
                    <div><a class="button" href="index.php?user=<?php echo $user['id']; ?>"><?php echo $user['email'];?></a></div>
                </li><?php
            } else { ?>
                <li class="inscription">
                    <div><a class="button" href="inscription.php">Inscription</a></div>
                </li>
                <li>
                    <div><a class="button" href="connexion.php">Connexion</a></div>
                </li>
            <?php
            } ?>
            <li>
                <div><a class="button" href="panier.php">Panier</a></div>
            </li>
            <li>
                <div><a class="carte" href="carte.php">Carte</a></div>
            </li><?php
            if(!empty($_SESSION['id'])){
                $verif_admin= $bdd->prepare('SELECT * FROM utilisateurs WHERE id = ?');
                $verif_admin->execute(array($_SESSION['id']));
                $user = $verif_admin->fetch();

                if($user['administrateur'] == 1) { ?>
                    <li>
                        <div><a class="ajout-vin" href="ajout_vin.php">Ajouter vin</a></div>
                     </li> <?php
                }
            } ?>
            <a href="index.php">
                <img class="logo" src="Capture_2.png">
            </a>
            <li>
                <div><a class="histoire" href="https://fr.wikipedia.org/wiki/Cit%C3%A9_du_Vin">Histoire</a>
                </div>
            </li>
            <li>
                <div class="select"><a class="accueil" href="index.php">Accueil</a></div>
            </li>
        </ul>
    </div>
    <div>
        <form method="post">
            <select class="ordre" value="Prix" name="prix" id="">
                <option class="ordre" value="">filtrer par prix</option>
                <option class="ordre" value="DESC">croissant</option>
                <option value="ASC">décroissant</option>
            </select>
            <input class="valide-ordre" type="submit" value="valider" name="valider">
        </form>
    </div><br><?php

        if($_SERVER['REQUEST_METHOD'] === 'POST') { 

            if(isset($_POST['prix'])) {

                $prix = $_POST['prix'];

                $select_vins = $bdd->query("SELECT * FROM vins WHERE id > 0 ORDER BY prix_vin ".$prix);
    
                while($vin = $select_vins->fetch()) { ?>
                    <div class="vin">
                        <a href="vin.php?id=<?php echo $vin['id']; ?>">
                            <img class="bouteilleVin" src="images/<?php echo $vin['nom_image']; ?><?php echo $vin['extension_image']; ?>" alt="">
                            <p class="nomVin"><?php echo $vin['nom_vin']; ?></p>
                            <p class="prixVin"><?php echo $vin['prix_vin']; ?> €</p>
                        </a>
                    </div>
                    <br>
                <?php 
                }
            }
        } else {?>
            <div class="all"> <?php
         
                $select_vins = $bdd->query('SELECT * FROM vins WHERE id > 0');
        
                while($vin = $select_vins->fetch()) { ?>
                    <div class="vin">
                        <a href="vin.php?id=<?php echo $vin['id']; ?>">
                            <img class="bouteilleVin"
                                src="images/<?php echo $vin['nom_image']; ?><?php echo $vin['extension_image']; ?>" alt="">
                            <p class="nomVin"><?php echo $vin['nom_vin']; ?></p>
                            <p class="prixVin"><?php echo $vin['prix_vin']; ?> €</p>
                        </a>
                    </div>
                    <br>

                <?php 
                }?>
            </div><?php
        }?>
    </div>
</body>

</html>