<?php

include('db.php');

if(isset($_SESSION['id'])) {

    $verif_utilisateur = $bdd->prepare('SELECT * FROM utilisateurs WHERE id = ?');
    $verif_utilisateur->execute(array($_SESSION['id']));
    $user = $verif_utilisateur->fetch();

    if($user['administrateur'] == 1) { 
        
        if(isset($_POST['valider'])) {

            if(!empty($_POST['nom_vin']) AND !empty($_POST['description_vin']) AND !empty($_POST['prix_vin']) AND !empty($_POST['chateau'])) {

                if($_SERVER['REQUEST_METHOD'] === 'POST') {

                    if(isset($_FILES['image']) AND !empty($_FILES['image']['name'])) {
                        foreach ($_FILES['image']['name'] as $key => $name) {

                            $chaine = uniqid();
                            $extension_upload = strrchr($_FILES['image']['name'][$key], '.');
                            $nom = "/Applications/MAMP/htdocs/cite_du_vin/cite-du-vin/images/{$chaine}{$extension_upload}";
                            $resultat = move_uploaded_file($_FILES['image']['tmp_name'][$key],$nom);

                            $insertion_produit = $bdd->prepare("INSERT INTO vins (nom_vin, prix_vin, description, chateau, nom_image, extension_image) VALUES (?,?,?,?,?,?)");
                            $insertion_produit->execute(array($_POST['nom_vin'], $_POST['prix_vin'] , $_POST['description_vin'], $_POST['chateau'], $chaine, $extension_upload));

                
                            if ($resultat){
                                header('Location: index.php');
                            }
                        }
                    } else { ?>
                        <p class="message">Erreur envoi de l'image</p>
                    <?php
                    }
                }
            } else { ?>
                <p class="message">Tous les champs doivent être rempli</p>
            <?php
            }
        }
        
        
        
        ?>

        <!DOCTYPE html>
        <html lang="fr">
        <head>
            <meta charset="UTF-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <link rel="stylesheet" href="/css/ajout_vin.css">
            <title>Ajout de vin</title>
        </head>
        <body>
                <div>
                    <ul class="topnav">
                        <li class="inscription">
                            <div><a class="button" href="inscription.php">Inscription</a></div>
                        </li>
                        <li>
                            <div><a class="button" href="connexion.php">Connexion</a></div>
                        </li>
                        <li>
                            <div><a class="button" href="panier.php">Panier</a></div>
                        </li>
                        <li>
                            <div><a class="carte" href="carte.php">Carte</a></div>
                        </li><?php
                        if($_SESSION['id']){
                            $verif_admin= $bdd->prepare('SELECT * FROM utilisateurs WHERE id = ?');
                            $verif_admin->execute(array($_SESSION['id']));
                            $user = $verif_admin->fetch();

                            if($user['administrateur'] == 1) { ?>
                                <li>
                                    <div class="select"><a class="ajout-vin" href="ajout_vin.php">Ajouter vin</a></div>
                                </li> <?php
                            }
                        } ?>
                        <a href="index.php">
                            <img class="logo" src="Capture_2.png">
                        </a>
                        <li>
                            <div><a class="histoire" href="https://fr.wikipedia.org/wiki/Cit%C3%A9_du_Vin">Histoire</a>
                            </div>
                        </li>
                        <li>
                            <div><a class="accueil" href="index.php">Accueil</a></div>
                        </li>
                    </ul>
                </div> 
            <div class="general">
                <form method="post" enctype="multipart/form-data">
                    <input class="nom" type="text" name="nom_vin" placeholder="Nom du vin">
                    <input class="prix" type="text" name="prix_vin" placeholder="Prix du vin">
                    <textarea class="desc" name="description_vin" cols="77" rows="7" placeholder="Description du vin"></textarea>
                    <input class="image" type="file" name="image[]" />
                    <input class="chateau" type="text" name="chateau" placeholder="Nom du chateau">
                    <input class="valider" type="submit" name="valider" value="Ajouter">
                </form>
            </div>
        </body>
        </html>

    <?php 
    } else {
        echo "Vous n'êtes pas administrateur";
    }
}
?>